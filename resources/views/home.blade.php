@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Two Factor authentication') }}</div>

                <div class="card-body">
                    @if (session('status') == "two-factor-authentication-disabled")
                        <div class="alert alert-success" role="alert">
                           Two factor authentication has been disabled
                        </div>
                    @endif
                    @if (session('status') == "two-factor-authentication-enabled")
                        <div class="alert alert-success" role="alert">
                           Two factor authentication has been enabled
                        </div>
                        <p class="text-center">Pleas scan this QR code in your authenticator</p>
                    @endif

                        <form action="/user/two-factor-authentication" method="POST">
                            @csrf
                            @if (auth()->user()->two_factor_secret)
                                @method('DELETE')
                                <div class="text-center">
                                    {!! auth()->user()->twoFactorQrCodeSvg() !!}
                                </div>
                                <div class="text-center pt-4">

                                    <button class="btn btn-danger text-center">Disable</button>
                                </div>
                            @else
                                <button class="btn btn-primary">Enable</button>
                            @endif

                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
