<!DOCTYPE html>
<html lang="en">

<head>
	<title>
		Limit Character
	</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
    <form>
        <p>Maximum input characters is 10</p>
        <input type="text" name="text" id="text" maxlength="10">
        <p>
            <span class="character">10</span>
            Characters Remaining
        </p>
	</form>
</body>
<script>
    $(document).ready(function () {
        var maxLength = 10;
        $('input').keyup(function () {
            var len = maxLength - $(this).val().length;
            $('.character').text(len);
        });

    });
</script>
</html>
