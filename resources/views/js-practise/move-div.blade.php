<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Move Div</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <div id="div1">
        <p>This paragraph move to id div2</p>
      </div>

      <div id="div2">
      </div>
</body>
<script>

    function init() {
        $('#div2').append( $('#div1>p') );
    }
    init();
    
</script>
</html>
