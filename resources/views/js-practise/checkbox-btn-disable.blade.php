<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Check box</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <form action="" id="form">

        <input type="checkbox" id="checkbox"><br><br>
        <input type='submit' value = 'submit' id='submit' class='enableOnInput' disabled="disabled" />
    </form>
</body>
<script>

$('#checkbox').click(function() {
	if ($('#submit').is(':disabled')) {
    	$('#submit').removeAttr('disabled');
    } else {
    	$('#submit').attr('disabled', 'disabled');
    }
});

</script>
</html>
