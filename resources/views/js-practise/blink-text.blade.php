<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blink Text</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>

    <p class="blink">Blink Text</p>

    <script>
        function blinkText() {
            $('.blink').fadeOut(200);
            $('.blink').fadeIn(200);
        }
        setInterval(blinkText, 500);
    </script>
</body>
</html>
