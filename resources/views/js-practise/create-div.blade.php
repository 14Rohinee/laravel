<!DOCTYPE html>
<html lang="en">

<head>
	<title>
		Create div
	</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    <div id="parent">
        This is parent div
    </div><br>
    <div></div>
    <input type="button" value="Create Div" onclick="createDiv()" />
    <script>
        function createDiv() {
            var object = {
                class: "divClass"
            };
            var newDiv = $("<div>", object);
            newDiv.html("Div Added");
            $("#parent").append(newDiv);
        }
    </script>
</body>

</html>
