<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>scroll</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <button id='scroll-bottom'>Go to bottom</button>

    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <p>scroll</p>
    <button id='scroll-top'>Go to top</button>
</body>

<script>
    // scroll to the top
    $('#scroll-top').click(function(){
        $('html, body').animate({scrollTop: 0}, 'slow');
    })

    // scroll to the bottom
   $('#scroll-bottom').click(function() {
        $("html, body").animate({ scrollTop: $(document).height() }, 'slow');
    });
</script>
</html>
