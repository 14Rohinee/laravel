<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Select value from json object</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <select id='name'>
        <option value="Red">Pihu</option>
        <option value="Blue">Siya</option>
        <option value="Green">Ritu</option>
    </select>
    <input type="button" value="Remove all Options" onclick="removeOption()"/>
</body>
<script>
    function removeOption()
    {
        $('#name').empty().append('<option selected="selected" value="test">Rohinee</option>');
    }

</script>
</html>
