<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Select value from json object</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <div id="div"></div>
</body>
<script>
    data = { "name": "Rohinee", "surname": "Tandekar", 'fullname': "Rohinee Tandekar" };
    $.each(data, function(key, value) {
        $('#div').append($("<option/>", {
            value: key,
            text: value
        }));
    });
</script>
</html>
