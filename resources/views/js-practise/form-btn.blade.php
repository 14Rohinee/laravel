<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form submit button</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <form action="" id="form">

        <input type='submit' value = 'disable' id='disable' class='enableOnInput'  />
        <input type='submit' value = 'enable' id='enable' class='enableOnInput' />
    </form>
</body>
<script>

    $(document).ready(function () {
        $("#form").submit(function (e) {
            //stop submitting the form to see the disabled button effect
            e.preventDefault();

            //disable the submit button
            $('#disable').attr('disabled', true);
        });
    });

</script>
</html>
