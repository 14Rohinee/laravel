@extends('layouts.admin')
@section('title')
    Client Details
@endsection

@section('bread-crumb')
<div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>{{ __('admin.Client Details') }}</h1>
        @role('client')
            <a href="{{ route('client-details.create') }}"  class="btn btn-primary mt" style="margin-left: 190px; margin-top:-50px"><span class="fa fa-plus"></span>{{ __('admin.Add') }}</a>
        @endrole
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">{{ __('admin.Home') }}</a></li>
          <li class="breadcrumb-item active">{{ __('admin.Client Details') }}</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
@endsection

@section('content')
    {!! $dataTable->table() !!}
@endsection

@section('script')
    {!! $dataTable->scripts() !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script>

    $(document).ready(function() {

        $('.delete-client').click(function() {

            let clientId = $(this).data('client-id');

            // Generate Token method 1
            // let token = $("meta[name='csrf-token']").attr("content");

            // Generate Token method 2
            let token = '{{ csrf_token() }}';

            let url = '{{ route('client-details.destroy', ':id') }}';
            url = url.replace(':id', clientId);

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function() {

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        "_token": token,
                        "_method": 'DELETE',
                    },
                    success: function() {
                        swal("Deleted!", "Client has been deleted.",
                            "success");
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                });

            });
        });

    });
</script>
@endsection
