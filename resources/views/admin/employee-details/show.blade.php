@extends('layouts.admin')

@section('title')
    Show Employee
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('bread-crumb')
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <a href="{{ route('employee-details.index') }}" class="btn btn-warning"><i class="nav-icon fas fa-arrow-left"></i>{{ __('admin.Go Back') }} </a>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">{{ __('admin.Home') }}</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('employee-details.index') }}">{{ __('admin.Employee Details') }}</a></li>
            <li class="breadcrumb-item active">{{ __('admin.Show Employee Details') }}</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3>{{ __('admin.Show Employee Details') }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="POST" action="{{ route('employee-details.show', $employeeDetail->id) }}" enctype="multipart/form-data">
                @csrf
              <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ __('admin.Gender') }}</label>
                            <div class="radio radio-inline">
                                <input type="radio" id="male" value="male" name="gender" checked="" value="male" {{ $employeeDetail->gender == 'male' ? 'checked' : '' }}>
                                <label for="male"> {{ __('admin.Male') }} </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" id="female" value="female" name="gender" value="female" {{ $employeeDetail->gender == 'female' ? 'checked' : '' }}>
                                <label for="female">{{ __('admin.Female') }}</label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" id="other" value="other" name="gender" value="other" {{ $employeeDetail->gender == 'other' ? 'checked' : '' }}>
                                <label for="other"> {{ __('admin.Other') }} </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone">{{ __('admin.Phone Number') }}</label>
                            <input type="phone" name="phone" class="form-control" id="phone" placeholder="Enter phone number"  value="{{ $employeeDetail->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="address">{{ __('admin.Address') }}</label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="Enter Address" value="{{ $employeeDetail->address }}">
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group ">
                            <label for="user_id">{{ __('admin.User') }}{{ __('admin.Name') }}</label>
                            <select class="form-control" name="user_id" id="user_id">
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}"@if($user->id == $employeeDetail->user_id) selected @endif  >{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-froup">
                            <label>{{ __('admin.Photo') }}</label>
                            <input type="file" class="dropify" name="file"  data-default-file="{{ asset('uploads/employee-uploads/employees/' . $employeeDetail->photo) }}"/>
                        </div>
                    </div>
                </div>
            </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection
