@extends('layouts.admin')
@section('title')
    Create Task
@endsection
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('bread-crumb')
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <a href="{{ route('tasks.index') }}" class="btn btn-warning"><i class="nav-icon fas fa-arrow-left"></i> {{ __('admin.Go Back') }}</a>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">{{ __('admin.Home') }}</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('tasks.index') }}">{{ __('admin.Tasks') }}</a></li>
            <li class="breadcrumb-item active">{{ __('admin.Create Task') }}</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3>{{ __('admin.Create Task') }} </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="POST" action="{{ route('tasks.store') }}">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">{{ __('admin.Name') }}</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group ">
                                <label for="client">{{ __('admin.Projects') }}</label>
                                <select class="form-control" name="project_id" id="project_id">
                                    @foreach ($projects as $project)
                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="due_date">{{ __('admin.Due Date') }}</label>
                                <input type="text" class="form-control datepicker" placeholder="Enter Due Date" name="due_date" id="due_date" autocomplete="off" value="{{ old('due_date') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="assigned_to">{{ __('admin.Assigned To') }}</label>
                                <select class="form-control" name="assigned_to" id="assigned_to">
                                    @foreach ($members as $member)
                                        <option value="{{ $member->id }}">{{ $member->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ __('admin.Status') }}</label>
                                <select class="form-control" name="status" id="status">
                                    <option>{{ __('admin.Select') }}</option>
                                    <option @if(old('status') == 'active') selected @endif value="active">{{ __('admin.Active') }}</option>
                                    <option  @if(old('status') == 'inactive') selected @endif value="inactive">{{ __('admin.Inactive') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">{{ __('admin.Submit') }}</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

    $('.datepicker').datepicker({
        // format: 'yyyy-mm-dd',
        format: 'dd-mm-yyyy',
        startView: 2,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
</script>
@endsection
