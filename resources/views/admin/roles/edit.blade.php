@extends('layouts.admin')
@section('title')
    Edit Role
@endsection
@section('bread-crumb')
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <a href="{{ route('roles.index') }}" class="btn btn-warning"><i class="nav-icon fas fa-arrow-left"></i> {{ __('admin.Go Back') }}</a>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">{{ __('admin.Home') }}</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">{{ __('admin.Roles') }}</a></li>
            <li class="breadcrumb-item active">{{ __('admin.Edit Role') }}</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3>{{ __('admin.Edit Role') }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="POST" action="{{ route('roles.update', $role->id) }}">
                @csrf
                @method('PUT')
              <div class="card-body">
                <div class="form-group">
                  <label for="name">{{ __('admin.Name') }}</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ $role->name }}">
                </div>
                <div class="form-group">
                  <label for="name">{{ __('admin.Display Name') }}</label>
                  <input type="text" name="display_name" class="form-control" id="displayname" placeholder="Enter display name" value="{{ $role->display_name }}">
                </div>
                <div>
                    <label for="name">{{ __('admin.Role') }}{{ __('admin.Description') }} </label>
                    <textarea name="description" id="description" cols="163" rows="5">
                        {{ $role->description }}
                    </textarea>
                </div>
                <div>
                    <label for="permissions">{{ __('admin.Permissions') }}</label>
                    <div>
                        @foreach ($permissions as $permission)
                            <input type="checkbox" {{ in_array($permission->id, $role_permission) ? 'checked' : '' }} name="permission[]" value="{{ $permission->id }}" >
                            <label for="permissions">{{ $permission->display_name }}</label>
                        @endforeach
                    </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">{{ __('admin.Submit') }}</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection
