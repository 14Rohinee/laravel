@extends('layouts.admin')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add Language Form</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/setting-index">Home</a>
            </li>
            <li class="active">fucnti
                <strong>Add Language</strong>

            </li>
        </ol>
    </div>
</div>

<body onload="languageModal()">
    <!-- Modal -->
    <div class="modal fade" id="editlanguage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Fill Form</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" class="form-horizontal" action="{{ route('languages.update', $language->id) }}">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" value="{{ $language->name }}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Code</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="code" value="{{ $language->code }}"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Select</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status" id="status">
                                    <option>Select</option>
                                    <option value="active" {{ $faqs->status == 'active' ? 'selected' : '' }}>
                                        Active</option>
                                    <option value="inactive"
                                        {{ $faqs->status == 'inactive' ? 'selected' : '' }}>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function languageModal() {
            $('#exampleModal').modal('show');
        }
    </script>
</body>

@endsection
