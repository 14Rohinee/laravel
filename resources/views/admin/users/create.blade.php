@extends('layouts.admin')
@section('title')
    Create User
@endsection
@section('bread-crumb')
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <a href="{{ route('users.index') }}" class="btn btn-warning"><i class="nav-icon fas fa-arrow-left"></i>{{ __('admin.Go Back') }}</a>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">{{ __('admin.Home') }}</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('users.index') }}">{{ __('admin.Users') }}</a></li>
            <li class="breadcrumb-item active">{{ __('admin.Create User') }}</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3>{{ __('admin.Create User') }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" method="POST" action="{{ route('users.store') }}">
                @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="name">{{ __('admin.Name') }}</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                  <label for="email">{{ __('admin.Email address') }}</label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email"  value="{{ old('email') }}">
                </div>
                <div class="form-group">
                  <label for="password">{{ __('admin.Password') }}</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">{{ __('admin.Submit') }}</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection
