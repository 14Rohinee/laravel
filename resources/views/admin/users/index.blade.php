@extends('layouts.admin')
@section('title')
    Users
@endsection
@section('bread-crumb')
<div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-3">
        <h1>{{ __('admin.Users List') }}</h1>
        <a href="{{ route('users.create') }}"  class="btn btn-primary mt" style="margin-left: 200px; margin-top:-50px"><span class="fa fa-plus"></span>{{ __('admin.Add') }}</a>
      </div>
      <div class="col-sm-3">
        <a href="/export"  class="btn btn-success mt" style=" margin-top:5px"><span class="fa fa-plus"></span>{{ __('admin.Export User') }}</a>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">{{ __('admin.Home') }}</a></li>
          <li class="breadcrumb-item active">{{ __('admin.User') }}</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
@endsection

@section('content')
    {!! $dataTable->table() !!}
@endsection

@section('script')
    {!! $dataTable->scripts() !!}
@endsection
