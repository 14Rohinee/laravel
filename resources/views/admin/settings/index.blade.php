@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 ">
                <div class="card card-primary card-tabs">
                  <div class="card-header p-0 pt-1">
                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">{{ __('admin.Language Settings') }}</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">2FA</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-one-general-tab" data-toggle="pill" href="#custom-tabs-one-general" role="tab" aria-controls="custom-tabs-one-general" aria-selected="false">{{ __('admin.General Settings') }}</a>
                      </li>
                    </ul>
                  </div>
                  <div class="card-body">
                    <div class="tab-content" id="custom-tabs-one-tabContent">
                      <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                            <div class="container-fluid">
                                <div class="row mb-2">
                                    <div class="col-md-1 col-sm-6">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hello">
                                            {{ __('admin.Add') }}
                                          </button>
                                    </div>
                                    <div class="col-sm-5">
                                        <a href="/translations" class="btn btn-warning" target="_blank"><i class="nav-icon fas fa-arrow-plus" ></i> {{ __('admin.Translations') }}</a>
                                    </div>
                                    <div class="col-sm-6">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item"><a href="#">{{ __('admin.Settings') }}</a></li>
                                        <li class="breadcrumb-item active"><a href="{{ route('users.index') }}">{{ __('admin.Language Settings') }}</a></li>
                                        <li class="breadcrumb-item active">{{ __('admin.Language List') }}</li>
                                    </ol>
                                    </div>
                                </div>
                                <div>
                                    {!! $dataTable->table() !!}
                                </div>
                            </div><!-- /.container-fluid -->
                        </div>

                      {{-- <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">

                      </div> --}}
                      {{-- <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">

                      </div> --}}
                      <div class="tab-pane fade" id="custom-tabs-one-general" role="tabpanel" aria-labelledby="custom-tabs-one-general-tab">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-12">
                                <ol class="breadcrumb text-center">
                                    <li class="breadcrumb-item"><a href="#">{{ __('admin.Settings') }}</a></li>
                                    <li class="breadcrumb-item active"><a href="#">{{ __('admin.General Settings') }}</a></li>
                                </ol>
                                </div>
                            </div>
                            <div>
                            <form id="quickForm" method="POST" action="{{ route('general-settings.update', $generalSettings->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group ">
                                                <label for="language">{{ __('admin.Language') }}</label>
                                                <select class="form-control" name="language" id="language">
                                                    @foreach ($languages as $language)
                                                        <option value="{{ $language->code }}" @if($language->code == $generalSettings->language) selected @endif>{{ $language->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">{{ __('admin.Submit') }}</button>
                                </div>
                                <!-- /.card-body -->
                            </form>
                        </div>
                    </div><!-- /.container-fluid -->
                </div>
            </div>
        </div>
                  <!-- /.card -->
    </div>
</div>
</div>
</div>
  <!-- Add language Modal -->

<div class="modal fade" id="hello" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h2 class="modal-title" id="exampleModalLabel">Add Language</h2>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <form method="POST" class="form-horizontal" action="{{ route('languages.store') }}">
              <div class="modal-body">
                  @csrf
                  <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">Code</label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="code"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                      <div class="col-sm-10">
                           <select class="form-control" name="status" id="status">
                              <option>Select</option>
                              <option @if(old('status') == 'active') selected @endif value="active">Active</option>
                              <option  @if(old('status') == 'inactive') selected @endif value="inactive">Inactive</option>
                          </select>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
      </div>
  </div>
</div>

{{-- Edit language modal--}}

{{-- <div class="modal fade" id="editlanguage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Edit Language</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" class="form-horizontal" action="{{ route('languages.update', $languages->id) }}">
                    <div class="modal-body">
                        @csrf
                        @method('PUT')
                        <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" value="{{ $language->name }}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Code</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="code" value="{{ $language->code }}"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Select</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status" id="status">
                                    <option>Select</option>
                                    <option  value="active" {{ $language->status == 'active' ? 'selected' : '' }}>
                                        Active</option>
                                    <option value="inactive" {{ $language->status == 'inactive' ? 'selected' : '' }}
                                    >Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
</div> --}}
@endsection

@section('script')
    {!! $dataTable->scripts() !!}
@endsection
