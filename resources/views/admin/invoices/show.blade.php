@extends('layouts.admin')
@section('title')
  Show Invoice
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection
@section('bread-crumb')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ route('invoices.index') }}" class="btn btn-warning"><i class="nav-icon fas fa-arrow-left"></i> {{ __('admin.Go Back') }}</a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">{{ __('admin.Home') }}</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('invoices.index') }}">{{ __('admin.Invoices') }}</a></li>
                <li class="breadcrumb-item active">{{ __('admin.Show Invoice') }}</li>
                </ol>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3>{{ __('admin.Show Invoice') }}</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" method="POST" action="{{ route('invoices.show', $invoice->id) }}">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label>{{ __('admin.Invoice Date') }}</label>
                                        <input type="text" class="form-control datepicker" placeholder="Enter Invoice Date" name="invoice_date" id="invoice_date" autocomplete="off" value="{{ $invoice->invoice_date }}">
                                        </div>
                                        <div class="form-group ">
                                            <label for="deadline">{{ __('admin.Due Date') }}</label>
                                            <input type="text" class="form-control datepicker" placeholder="Enter Due Date" name="due_date" id="due_date" autocomplete="off" value="{{ $invoice->due_date }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label for="completions">{{ __('admin.Client') }}</label>
                                            <select class="form-control" name="client_id" id="client_id">
                                                <option>{{ __('admin.Select') }}</option>
                                                @foreach ($clients as $client)
                                                    <option value="{{ $client->id }}"  @if($client->id == $invoice->client_id) selected @endif>{{ $client->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group ">
                                            <label for="client">{{ __('admin.Project') }}</label>
                                            <select class="form-control" name="project_id" id="project_id">
                                                <option>{{ __('admin.Select') }}</option>
                                                @foreach ($projects as $project)
                                                    <option value="{{ $project->id }}" @if($project->id == $invoice->project_id) selected @endif>{{ $project->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <a href="/stripe" class="btn btn-primary">{{ __('admin.Pay') }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>

        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
    </script>
@endsection
