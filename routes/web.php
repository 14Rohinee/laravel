<?php

use App\Http\Controllers\ClientDetailController;
use App\Http\Controllers\EmployeeDetailController;
use App\Http\Controllers\GeneralSettingController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Stripe\StripeClient;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
})->middleware('auth');

// js routes
Route::get('/check-jquery', function(){
    return view('check-jquery-loaded');
});
Route::get('/scroll', function(){
    return view('scroll');
});
Route::get('/disable-right-click', function(){
    return view('disable-right-click');
});
Route::get('/disable-btn', function(){
    return view('form-btn');
});
Route::get('/checkbox-disable', function(){
    return view('checkbox-btn-disable');
});
Route::get('/broken-image', function(){
    return view('broken-image');
});
Route::get('/blink-text', function(){
    return view('blink-text');
});
Route::get('/print', function(){
    return view('print-page');
});
Route::get('/limit', function(){
    return view('limit-character');
});
Route::get('/bold', function(){
    return view('first-word-bold');
});
Route::get('/create-div', function(){
    return view('create-div');
});
Route::get('/move-div', function(){
    return view('move-div');
});
Route::get('/json', function(){
    return view('json');
});
Route::get('/remove-option', function(){
    return view('remove-option');
});

// admin routes
Route::get('/', function(){
    return view('auth.login');
});

Route::group([ 'middleware' => ['role:admin|employee|client']], function() {
    Route::get('settings', function(){
        return view('admin.settings.index');
    });
    Route::get('settings', [SettingController::class, 'index']);
    Route::resource('general-settings', GeneralSettingController::class);
    Route::resource('languages', LanguageController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('projects', ProjectController::class);
    Route::resource('tasks', TaskController::class);
    Route::resource('client-details', ClientDetailController::class);
    Route::resource('employee-details', EmployeeDetailController::class);
    Route::get('/export', [UserController::class, 'export']);

});
Route::group([ 'middleware' => ['role:admin|client|employee']], function() {
    Route::get('index', function(){
        return view('admin.index');
    });
    Route::resource('client-details', ClientDetailController::class);
    Route::resource('projects', ProjectController::class);
    // Route::resource('stripe', StripeController::class);
    Route::get('stripe', [StripeController::class, 'index']);
    Route::post('projects',  [StripeController::class, 'purchase'])->name('projects.purchase');
});
Route::group([ 'middleware' => ['role:admin|client']], function() {
    Route::resource('invoices', InvoiceController::class);
});
