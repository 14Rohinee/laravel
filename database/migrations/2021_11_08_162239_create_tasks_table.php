<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('project_id')->unique();
            $table->foreign('project_id')->references('project_id')->on('project_user')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->unsignedBigInteger('assigned_to')->unique();
            $table->foreign('assigned_to')->references('user_id')->on('project_user')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('name');
            $table->dateTime('due_date');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
