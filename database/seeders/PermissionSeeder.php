<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions =  ([
            [
              'id' => 1,
              'name' => 'create-user',
              'display_name' => 'Create User',
              'description'=> 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            ],

            [
              'id' => 2,
              'name' => 'show-user',
              'display_name' => 'Show User',
              'description'=> 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            ],

            [
              'id' => 3,
              'name' => 'edit-user',
              'display_name' => 'Edit User',
              'description'=> 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            ],
            [
              'id' => 4,
              'name' => 'delete-user',
              'display_name' => 'Delete User',
              'description'=> 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            ],
            [
              'id' => 5,
              'name' => 'create-client',
              'display_name' => 'Create Client',
              'description'=> 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            ],
            [
              'id' => 6,
              'name' => 'show-client',
              'display_name' => 'Show Client',
              'description'=> 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            ],
            [
              'id' => 7,
              'name' => 'edit-client',
              'display_name' => 'Edit Client',
              'description'=> 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.',
            ],

          ]);

          collect($permissions)->each(function ($permission) {
            Permission::create($permission);
        });
    }
}
