<?php
namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles =  ([
           [
             'id' => 1,
             'name' => 'admin',
             'display_name' => 'Admin',
             'description' => 'Admin can handle employees',
           ],
           [
             'id' => 2,
             'name' => 'employee',
             'display_name' => 'Employee',
             'description' => 'Employee can handle client',
           ],
           [
             'id' => 3,
             'name' => 'client',
             'display_name' => 'Client',
             'description' => 'Client can handle their profile',
           ],

         ]);

         collect($roles)->each(function ($role) {
           Role::create($role);
       });
    }
}
