<?php
namespace App\DataTables;

use App\Models\Role;
use Yajra\DataTables\Services\DataTable;

class RoleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('permissions', function($row)
            {
                $permissions = '';
                foreach ($row->permissions as $key => $permission) {
                    $permissions .= $permission->name . ',';
                }
                return $permissions;
            })
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('roles.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i>
                    </a>

                    <a class="btn btn-warning" href="' . route('roles.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-danger delete-role" href="javascript:;" data-user-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                    </a>';
                return $action;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Role $roles)
    {
        $roles = Role::select();
        return $this->applyScopes($roles);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('role-table')
                    ->columns([
                        'name',
                        'display_name',
                        'description',
                        'permissions',
                        'action'
                    ]);

    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name',
            'display_name',
            'description',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Role_' . date('YmdHis');
    }
}
