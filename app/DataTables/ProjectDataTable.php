<?php

namespace App\DataTables;

use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Trebol\Entrust\Entrust;
use Yajra\DataTables\Services\DataTable;

class ProjectDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('Members', function ($row) {
                $members = '';
                foreach ($row->members as $member) {
                    $members .= $member->name . ',';
                }
                return $members;
            })
            ->addColumn('client', function ($row) {
                return $row->client->name;
            })
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('projects.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i>
                    </a>

                    <a class="btn btn-warning" href="' . route('projects.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-danger delete-project" href="javascript:;" data-project-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                    </a>';
                return $action;
            });
    }
    public function query(Project $projects)
    {
        $projects = Project::with('members')->select();
        return $this->applyScopes($projects);
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('project-table')
            ->columns([
                'id',
                'name',
                'Members',
                'deadline',
                'client',
                'completions',
                'status',
                'action',
            ]);
    }
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'deadline',
            'client',
            'completions',
            'status',
        ];
    }
    protected function filename()
    {
        return 'Project_' . date('YmdHis');
    }
}
