<?php

namespace App\DataTables;

use App\Models\Invoice;
use Yajra\DataTables\Services\DataTable;

class InvoiceDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('project', function ($row) {
                return $row->project->name;

            })
            ->addColumn('client', function ($row) {
                return $row->users->name;
            })
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('invoices.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i>
                    </a>

                    <a class="btn btn-primary" href="/stripe">
                    Pay
                    </a>

                    <a class="btn btn-warning" href="' . route('invoices.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-danger delete-project" href="javascript:;" data-project-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                    </a>';
                return $action;
            });
    }
    public function query(Invoice $invoice)
    {
        $invoice = Invoice::with('users')->select();
        return $this->applyScopes($invoice);
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('project-table')
            ->columns([
                'id',
                'project',
                'client',
                'invoice_date',
                'due_date',
                'action',
            ]);
    }
    protected function getColumns()
    {
        return [
            'id',
            'project',
            'client',
            'invoice_date',
            'due_date',
        ];
    }
    protected function filename()
    {
        return 'Invoice' . date('YmdHis');
    }
}
