<?php
namespace App\DataTables;

use App\Models\ClientDetail;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;

class clientDetailDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('name', function ($row) {
                return $row->users->name;
            })
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('client-details.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i>
                    </a>
                    <a class="btn btn-warning" href="' . route('client-details.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>
                    ';
                    if(!(Auth::user()->can(['create-client']))
                    ) {
                        $action .= '<a class="btn btn-danger delete-client" href="javascript:;" data-client-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                        </a>';
                    }
                return $action;
            });
    }
    public function query(ClientDetail $clientDetails)
    {
        $clientDetails = ClientDetail::select();
        return $this->applyScopes($clientDetails);
    }

    public function html()
    {
        return $this->builder()
        ->setTableId('clientDetail-table')
        ->columns([
            'id',
            'name',
            'company_name',
            'phone',
            'photo',
            'address',
            'action',
        ]);
    }
    protected function getColumns()
    {
        return [
            'id',
            'company_name',
            'phone',
            'photo',
            'address',
        ];
    }
    protected function filename()
    {
        return 'ClientDetail' . date('YmdHis');
    }
}
