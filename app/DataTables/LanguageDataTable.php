<?php
namespace App\DataTables;

use App\Models\Language;
use Yajra\DataTables\Services\DataTable;

class LanguageDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '
                    <a class="btn btn-primary" href="' . route('languages.edit', [$row->id]) . '"  data-toggle="modal" data-target="#editlanguage">
                    <i class="fa fa-eye "></i>
                    </a>
                    <a class="btn btn-danger delete-user" href="javascript:;" data-user-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                    </a>';
                return $action;
            });

    }
    public function query(Language $languages)
    {
        $languages = Language::select();
        return $this->applyScopes($languages);
    }

    public function html()
    {
        return $this->builder()
        ->setTableId('language-table')
        ->columns([
                'name',
                'code',
                'status',
                'action',
        ]);
    }
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'code',
            'status',
        ];
    }
    protected function filename()
    {
        return 'Language' . date('YmdHis');
    }
}
