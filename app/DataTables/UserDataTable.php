<?php

namespace App\DataTables;

use App\Models\User;
use Illuminate\Support\Facades\Request;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('role', function ($row) {
                $roles = '';
                foreach ($row->roles as $key => $role) {
                    $roles .= $role->name . ' ';
                }
                return $roles;
            })
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('users.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i>
                    </a>

                    <a class="btn btn-warning" href="' . route('users.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-danger delete-user" href="javascript:;" data-user-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                    </a>';
                return $action;
            });
    }
    public function query(User $users)
    {
        $users = User::with('roles')->select();
        return $this->applyScopes($users);
    }
    public function html()
    {
        return $this->builder()
            ->setTableId('user-table')
            ->columns([
                'id',
                'name',
                'email',
                'role',
                'action'

            ]);
    }
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'role',
            'email'
        ];
    }
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
