<?php

namespace App\DataTables;

use App\Models\Task;
use Yajra\DataTables\Services\DataTable;

class TaskDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('project', function ($row) {
                return $row->project->name;
            })
            ->addColumn('assigned_to', function ($row) {
                return $row->assignedTo->name;
            })
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('tasks.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i>
                    </a>

                    <a class="btn btn-warning" href="' . route('tasks.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-danger delete-user" href="javascript:;" data-user-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                    </a>';
                return $action;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Task $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Task $projects)
    {
        $projects = Task::with('project')->select();
        return $this->applyScopes($projects);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('task-table')
            ->columns([
                'id',
                'name',
                'project',
                'due_date',
                'assigned_to',
                'status',
                'action',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'project',
            'due date',
            'assigned_to',
            'status',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Task_' . date('YmdHis');
    }
}
