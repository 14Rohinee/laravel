<?php
namespace App\DataTables;

use App\Models\EmployeeDetail;
use App\Models\User;
use Yajra\DataTables\Services\DataTable;

class EmployeeDetailDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('name', function ($row) {
                return $row->users->name;
            })
            ->addColumn('action', function ($row) {
                $action = '<a class="btn btn-primary" href="' . route('employee-details.show', [$row->id]) . '">
                    <i class="fa fa-eye "></i>
                    </a>

                    <a class="btn btn-warning" href="' . route('employee-details.edit', [$row->id]) . '">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a class="btn btn-danger delete-user" href="javascript:;" data-user-id="' . $row->id . '">
                        <i class="fa fa-trash"></i>
                    </a>';
                return $action;
            });
    }
    public function query(EmployeeDetail $employeeDetails)
    {
        $employeeDetails = EmployeeDetail::with('users')->select();
        return $this->applyScopes($employeeDetails);
    }

    public function html()
    {
        return $this->builder()
        ->setTableId('employeeDetail-table')
        ->columns([
            'id',
            'name',
            'gender',
            'phone',
            'photo',
            'address',
            'action',
        ]);
    }
    protected function getColumns()
    {
        return [
            'id',
            'gender',
            'phone',
            'photo',
            'address',
        ];
    }
    protected function filename()
    {
        return 'EmployeeDetail' . date('YmdHis');
    }
}
