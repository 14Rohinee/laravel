<?php

namespace App\Observers;

use App\Models\EmployeeDetail;
use Illuminate\Support\Facades\File;

class EmployeeDetailObserver
{
    public function creating(EmployeeDetail $employee)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $employee->photo = $file_name; // Save file name to database
        }
    }

    public function created(EmployeeDetail $employee)
    {
        if (request()->file) {
            $path = public_path('uploads/employee-uploads/employees');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $employee->photo);
        }
    }

    public function saving(EmployeeDetail $employee)
    {
        if (request()->file) {
            // Old file delete code
            $path = public_path('uploads/employee-uploads/employees/');
            $this->deleteFile($path . $employee->photo);

            $file_name = time() . '.' . request()->file->extension();
            $employee->photo = $file_name; // Save file name to database
        }
    }

    public function updated(EmployeeDetail $employee)
    {
        if (request()->file) {

            $path = public_path('uploads/employee-uploads/employees/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $employee->photo);
        }
    }

    public function deleted(EmployeeDetail $employee)
    {
        $path = public_path('uploads/employee-uploads/employees/');

        // Old file delete code
        $this->deleteFile($path . $employee->photo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}
