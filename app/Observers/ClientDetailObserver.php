<?php

namespace App\Observers;

use App\Models\ClientDetail;
use Illuminate\Support\Facades\File;

class ClientDetailObserver
{
    public function creating(ClientDetail $client)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $client->photo = $file_name; // Save file name to database
        }
    }

    public function created(ClientDetail $client)
    {
        
        if (request()->file) {
            $path = public_path('uploads/client-uploads/clients');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $client->photo);
        }
    }

    public function saving(ClientDetail $client)
    {
        if (request()->file) {
            // Old file delete code
            $path = public_path('uploads/client-uploads/clients/');
            $this->deleteFile($path . $client->photo);

            $file_name = time() . '.' . request()->file->extension();
            $client->photo = $file_name; // Save file name to database
        }
    }

    public function updated(ClientDetail $client)
    {
        if (request()->file) {

            $path = public_path('uploads/client-uploads/clients/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $client->photo);
        }
    }

    public function deleted(ClientDetail $client)
    {
        $path = public_path('uploads/client-uploads/clients/');

        // Old file delete code
        $this->deleteFile($path . $client->photo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}
