<?php

namespace App\Models;

use App\Observers\ClientDetailObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Trebol\Entrust\Traits\EntrustUserTrait;

class ClientDetail extends Model
{
    use HasFactory, EntrustUserTrait;

    protected $fillable = [

        'user_id',
        'company_name',
        'phone',
        'photo',
        'address',
    ];

    protected static function boot()
    {
        parent::boot();
        static::observe(ClientDetailObserver::class);
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function permissions()
    {
        return $this->belongsTo(Permission::class, 'user_id');
    }
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }
}
