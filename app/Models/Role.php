<?php namespace App\Models;

use Illuminate\Support\Facades\DB;
use Trebol\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ['name', 'display_name', 'description'];

    public function users() {
        return $this->hasMany(User::class, 'role_user');
    }
    public function clients() {
        return $this->hasMany(ClientDetail::class, 'role_user');
    }
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    public static function getRole()
    {
        $records = DB::table('roles')->select('name')->get()->toArray();
        return $records;
    }
}
