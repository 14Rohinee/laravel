<?php namespace App\Models;

use Trebol\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
   public function roles()
   {
       return $this->belongsToMany(Role::class);
   }
   public function clientDetails()
   {
       return $this->hasMany(ClientDetail::class);
   }
}
