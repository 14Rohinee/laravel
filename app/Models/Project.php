<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'client_id', 'name', 'deadline', 'completions', 'user_id'];


    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] = date('Y-m-d', strtotime($value));
    }

    // Accessor
    public function getDeadlineAttribute($value)
    {
        return date('d M, Y', strtotime($value));
    }
    public function users()
    {
        return $this->belongsToMany(User::class ,'project_user');
    }
    public function clientDetail()
    {
        return $this->belongsTo(ClientDetail::class);
    }
    public function members()
    {
        return $this->belongsToMany(User::class, 'project_user');
    }

    public function client()
    {
        return $this->belongsTo(User::class);
    }
    public function invoice()
    {
        return $this->hasMany(Invoice::class);
    }

    // relation with tsak table

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    // public function tasks()
    // {
    //     return $this->belongsTo(Task::class);
    // }

}

