<?php

namespace App\Models;

use App\Observers\EmployeeDetailObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'gender',
        'phone',
        'photo',
        'address',
    ];
    protected static function boot()
    {
        parent::boot();
        static::observe(EmployeeDetailObserver::class);
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
