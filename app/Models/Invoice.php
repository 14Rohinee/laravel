<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    public function users()
    {
        return $this->belongsTo(User::class, 'client_id');
    }
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
    public function setInvoiceDateAttribute($value)
    {
        $this->attributes['invoice_date'] = date('Y-m-d', strtotime($value));
    }
    // Accessor
    public function getInvoiceDateAttribute($value)
    {
        return date('d M, Y', strtotime($value));
    }

    public function setDueDateAttribute($due_date)
    {
        $this->attributes['due_date'] = date('Y-m-d', strtotime($due_date));
    }
    public function getDueDateAttribute($due_date)
    {
        return date('d M, Y', strtotime($due_date));
    }
}
