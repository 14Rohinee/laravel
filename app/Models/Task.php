<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'name', 'project_id', 'due_date', 'assigned_to', 'status'];

    public function setDueDateAttribute($value)
    {
        $this->attributes['due_date'] = date('Y-m-d', strtotime($value));
    }

    // Accessor
    public function getDueDateAttribute($value)
    {
        return date('d M, Y', strtotime($value));
    }

    // relation with projects table
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    // relation with user table
    public function assignedTo()
    {
        return $this->belongsTo(User::class, 'assigned_to', 'id');
    }
  
}
