<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Sanctum\HasApiTokens;
use Trebol\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable, TwoFactorAuthenticatable, HasApiTokens, HasFactory, EntrustUserTrait, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'email_verified_at',
    ];
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    public function clientDetails()
    {
        return $this->hasOne(ClientDetail::class);
    }
    public function employeeDetails()
    {
        return $this->hasOne(EmployeeDetail::class);
    }
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    public function invoice()
    {
        return $this->hasMany(Invoice::class);
    }

    public static function getUser()
    {
        // $recoreds = DB::table('users')->select('id', 'name', 'email', 'password')->get()->to;
        $recoreds = User::with('roles')->get();
        return $recoreds;
    }

    public function can($permission, $requireAll = false)
    {
        config(['cache.default' => 'array']);

        if (is_array($permission)) {
            foreach ($permission as $permName) {
                $hasPerm = $this->can($permName);

                if ($hasPerm && !$requireAll) {
                    return true;
                }

                if (!$hasPerm && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
            // Return the value of $requireAll;
            return $requireAll;
        }
        else {
            foreach ($this->cachedRoles() as $role) {
                // Validate against the Permission table
                foreach ($role->cachedPermissions() as $perm) {
                    if (Str::is($permission, $perm->name)) {
                        return true;
                    }
                }
            }
        }

        config(['cache.default' => 'file']);
        return false;
    }
}
