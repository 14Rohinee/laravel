<?php

namespace App\Http\Controllers;

use App\DataTables\LanguageDataTable;
use App\Models\GeneralSetting;
use App\Models\Language;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    public function index(LanguageDataTable $dataTable)
    {
        $languages = Language::get();
        // dd($languages);
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $generalSettings = GeneralSetting::first();
        // dd($dataTable->render('admin.settings.index', compact('languages', 'code', 'generalSettings', 'languages')));
        return $dataTable->render('admin.settings.index', compact('languages', 'code', 'generalSettings', 'languages'));

    }
}

