<?php

namespace App\Http\Controllers;

use App\DataTables\LanguageDataTable;
use App\Models\GeneralSetting;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function index(LanguageDataTable $dataTable)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        return $dataTable->render('admin.settings.index', compact('code'));

    }

    public function create()
    {
        return view('dashboard.languages.create');
    }

    public function store(Request $request)
    {
        $language = new Language;
        $language->name = $request->name;
        $language->code = $request->code;
        $language->status = $request->status;
        $path = base_path() . '/resources/lang/' . $request->code;
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $language->save();
        return redirect('/settings');
    }

    public function edit($id)
    {
        $language = Language::find($id);
        return view('admin.settings.index', compact('language'));
    }

    public function update(Request $request, $id)
    {
        $language = Language::find($id);
        $language->name = $request->name;
        $language->code = $request->code;
        $language->status = $request->status;
        $language->save();
        return redirect('/settings');
    }

    public function destroy($id)
    {
        $code = Language::select('code')->where('id',$id )->pluck('code');
        if($code[0] =='en'){
            return redirect('/settings');
        }
        $path = base_path().'/resources/lang/'.$code[0];
        if (file_exists($path)) {
              File::deleteDirectory($path);
          }
        Language::find($id)->delete();
        return redirect('/settings');

    }
}
