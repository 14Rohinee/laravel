<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\InvoiceDataTable;
use App\Models\GeneralSetting;
use App\Models\Invoice;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(InvoiceDataTable $dataTable)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        return $dataTable->render('admin.invoices.index', compact('code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $clients = User::whereHas('roles', function($q)
        {
            $q->where('name', 'client');
        })->get();
        $projects = Project::where('status', 'active')->get();
        return view('admin.invoices.create', compact('code', 'clients', 'projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invoice = new Invoice();
        $invoice->client_id = $request->client_id;
        $invoice->project_id = $request->project_id;
        $invoice->invoice_date = $request->invoice_date;
        $invoice->due_date = $request->due_date;
        $invoice->save();
        return redirect('/invoices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $invoice = Invoice::find($id);
        $clients = User::whereHas('roles', function($q)
        {
            $q->where('name', 'client');
        })->get();
        $projects = Project::where('status', 'active')->get();
        return view('admin.invoices.show', compact('code', 'clients', 'projects', 'invoice'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $invoice = Invoice::find($id);
        $clients = User::whereHas('roles', function($q)
        {
            $q->where('name', 'client');
        })->get();
        $projects = Project::where('status', 'active')->get();
        return view('admin.invoices.edit', compact('code', 'clients', 'projects', 'invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $invoice = Invoice::find($id);
        $invoice->client_id = $request->client_id;
        $invoice->project_id = $request->project_id;
        $invoice->invoice_date = $request->invoice_date;
        $invoice->due_date = $request->due_date;
        $invoice->save();
        return redirect('/invoices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::find($id)->delete();
    }
}
