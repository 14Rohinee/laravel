<?php

namespace App\Http\Controllers;

use App\DataTables\EmployeeDetailDataTable;
use App\Models\EmployeeDetail;
use App\Models\GeneralSetting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EmployeeDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EmployeeDetailDataTable $dataTable)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        return $dataTable->render('admin.employee-details.index', compact('code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $users = User::whereHas(
            'roles', function($q){
                $q->where('name', 'employee');
            }
        )->get();
        return view('admin.employee-details.create', compact('users', 'code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employeeDetail = new EmployeeDetail;
        $employeeDetail->user_id = $request->user_id;
        $employeeDetail->gender = $request->gender;
        $employeeDetail->phone = $request->phone;
        $employeeDetail->address = $request->address;
        $employeeDetail->save();
        return redirect('/employee-details')->with(['success' => 'Client has been created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeDetail  $employeeDetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $employeeDetail = EmployeeDetail::find($id);
        $users = User::whereHas(
            'roles', function($q){
                $q->where('name', 'employee');
            }
        )->get();
        return view('admin.employee-details.show', compact('employeeDetail', 'users', 'code'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeDetail  $employeeDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $employeeDetail = EmployeeDetail::find($id);
        $users = User::whereHas(
            'roles', function($q){
                $q->where('name', 'employee');
            }
        )->get();
        return view('admin.employee-details.edit', compact('employeeDetail', 'users', 'code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeDetail  $employeeDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employeeDetail = EmployeeDetail::find($id);
        $employeeDetail->user_id = $request->user_id;
        $employeeDetail->gender = $request->gender;
        $employeeDetail->phone = $request->phone;
        $employeeDetail->address = $request->address;
        $employeeDetail->save();
        return redirect('/employee-details')->with(['success' => 'Client has been created.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeDetail  $employeeDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EmployeeDetail::find($id)->delete();
    }
}
