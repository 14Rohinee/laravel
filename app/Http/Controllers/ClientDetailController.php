<?php

namespace App\Http\Controllers;

use App\DataTables\ClientDetailDataTable;
use App\Models\ClientDetail;
use App\Models\GeneralSetting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClientDetailController extends Controller
{
    // function __construct()
    // {
    //      $this->middleware('permission:create-user|create-client|show-client|edit-client', ['only' => ['create','show','edit','update']]);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClientDetailDataTable $dataTable)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        return $dataTable->render('admin.client-details.index', compact('code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $users = User::whereHas(
            'roles', function($q){
                $q->where('name', 'client');
            }
        )->get();
        return view('admin.client-details.create', compact('users', 'code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientDetail = new ClientDetail;
        $clientDetail->company_name = $request->company_name;
        $clientDetail->phone = $request->phone;
        $clientDetail->address = $request->address;
        $clientDetail->user_id = $request->user_id;
        $clientDetail->save();
        return redirect('/client-details')->with(['success' => 'Client has been created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClientDetail  $clientDetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $clientDetail = ClientDetail::find($id);
        $users = User::whereHas(
            'roles', function($q){
                $q->where('name', 'client');
            }
        )->get();
        return view('admin.client-details.show', compact('clientDetail', 'users', 'code'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClientDetail  $clientDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $clientDetail = ClientDetail::find($id);
        $users = User::whereHas(
            'roles', function($q){
                $q->where('name', 'client');
            }
        )->get();
        return view('admin.client-details.edit', compact('clientDetail', 'users', 'code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClientDetail  $clientDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clientDetail = ClientDetail::find($id);
        $clientDetail->company_name = $request->company_name;
        $clientDetail->phone = $request->phone;
        $clientDetail->address = $request->address;
        $clientDetail->user_id = $request->user_id;
        $clientDetail->save();
        return redirect('/client-details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClientDetail  $clientDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientDetail $id)
    {
        ClientDetail::find($id)->delete();

    }
}
