<?php
namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GeneralSettingController extends Controller
{
    public function index()
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $languages = Language::get();
        return view('admin.settings.index', compact('generalSetting', 'languages', 'code'));
    }

    public function edit($id)
    {
        $generalSetting = GeneralSetting::find($id);
        return view('admin.settings.index', compact('generalSetting'));
    }

    public function update(Request $request, $id)
    {
        $generalSetting = GeneralSetting::find($id);
        $generalSetting->language = $request->language;
        $generalSetting->save();
        return redirect('/settings');
    }

}
