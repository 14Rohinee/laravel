<?php

namespace App\Http\Controllers;

use App\DataTables\TaskDataTable;
use App\Models\GeneralSetting;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TaskDataTable $taskDataTable)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        return $taskDataTable->render('admin.tasks.index', compact('code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $projects = Project::with('members')->get();
        foreach ($projects as $key => $project) {
            $members = $project->members;
        }
        return view('admin.tasks.create', compact('projects', 'members', 'code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task;
        $task->name = $request->name;
        $task->assigned_to = $request->assigned_to;
        $task->project_id = $request->project_id;
        $task->due_date = $request->due_date;
        $task->status = $request->status;
        $task->save();
        return redirect('/tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $task = Task::find($id);
        $projects = Project::with('members')->get();
        foreach ($projects as $key => $project) {
            $members = $project->members;
        }
        return view('admin.tasks.show', compact('projects', 'members', 'task', 'code'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $task = Task::find($id);
        $projects = Project::with('members')->get();
        foreach ($projects as $key => $project) {
            $members = $project->members;
        }
        return view('admin.tasks.edit', compact('projects', 'members', 'task', 'code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $task = Task::find($id);
        $task->name = $request->name;
        $task->assigned_to = $request->assigned_to;
        $task->project_id = $request->project_id;
        $task->due_date = $request->due_date;
        $task->status = $request->status;
        $task->save();
        return redirect('/tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::find($id)->delete();
    }
}
