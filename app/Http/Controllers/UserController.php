<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\DataTables\UserDataTable;
use App\Exports\UsersExport;
use App\Models\GeneralSetting;
use App\Models\Role;
use Illuminate\Support\Facades\Session;
use Laravel\Cashier\Cashier;
use Maatwebsite\Excel\Facades\Excel;
use Trebol\Entrust\Entrust;

class UserController extends Controller
{

    public function index(UserDataTable $dataTable)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));

        return $dataTable->render('admin.users.index', compact('code'));
    }

    public function create()
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        return view('admin.users.create', compact('code'));
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        $user->attachRole(Role::where('name','employee')->first());
        $user->createAsStripeCustomer();
        return redirect('/users')->with(['success' => 'User has been created.']);
    }

    public function show($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $user  = User::find($id);
        return view('admin.users.show', compact('user', 'code'));
    }

    public function edit($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $user  = User::find($id);
        return view('admin.users.edit', compact('user', 'code'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect('/users')->with(['success' => 'User has been created.']);
    }

    public function destroy($id){

        $user  = User::find($id);
        if ($user->user()->role() == 'admin' && $user->role() == 'employee' && $user->role() == 'client') {
            abort(401, $user +'can not be delete');
        }else{
            $user->delete();
        }
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
