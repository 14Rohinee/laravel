<?php

namespace App\Http\Controllers;

use App\DataTables\ProjectDataTable;
use App\Models\GeneralSetting;
use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Trebol\Entrust\Entrust;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProjectDataTable $dataTable)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        return $dataTable->render('admin.projects.index', compact('code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $members = User::whereHas(
            'roles', function($q){
                $q->where('name', 'employee');
            }
        )->get();
        $clients = User::whereHas(
            'roles', function($q){
                $q->where('name', 'client');
            }
        )->get();
        return view('admin.projects.create', compact('members', 'clients', 'code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = new Project;
        $project->client_id = $request->client_id;
        $project->name = $request->name;
        $project->deadline = $request->deadline;
        $project->completions = $request->completions;
        $project->status = $request->status;
        $project->price = $request->price;
        $user_id = $request->user_id;
        $project->save();
        $project->members()->attach($user_id);
        return redirect('/projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $members = User::whereHas(
            'roles', function($q){
                $q->where('name', 'employee');
            }
        )->get();
        $clients = User::whereHas(
            'roles', function($q){
                $q->where('name', 'client');
            }
        )->get();
        $project = Project::find($id);
        
        return view('admin.projects.show', compact('members', 'clients', 'project', 'code', 'intent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $members = User::whereHas(
            'roles', function($q){
                $q->where('name', 'employee');
            }
        )->get();
        $clients = User::whereHas(
            'roles', function($q){
                $q->where('name', 'client');
            }
        )->get();
        $project = Project::find($id);
        return view('admin.projects.edit', compact('members', 'clients', 'project', 'code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $project->client_id = $request->client_id;
        $project->name = $request->name;
        $project->deadline = $request->deadline;
        $project->completions = $request->completions;
        $project->status = $request->status;
        $project->price = $request->price;
        $user_id = $request->user_id;
        $project->save();
        $project->members()->attach($user_id);
        return redirect('/projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::find($id)->delete();
    }
}
